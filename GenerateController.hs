module Main
where

import Text.XML.HXT.Core
import Text.HandsomeSoup
import System.IO
import System.Environment

main = do
        mvcConfig <- readFile "controller/mvc.xml"
        let doc = readString [] mvcConfig
	resources <- runX $ doc /> hasName "mvc" /> hasName "resources" /> hasName "pattern" /> getText
	actions <- runX $ doc /> hasName "mvc" /> hasName "action" >>> (getAttrValue "url" &&& getAttrValue "module" &&& getAttrValue "method" &&& (this /> hasName "results" /> hasName "result" >>> (getAttrValue "value" &&& this /> getText ) ) )  
	modules <- runX $ doc /> hasName "mvc" /> hasName "action" >>> (getAttrValue "module" )  
	let imports = "import View\n" ++ generateImports modules
	let generated = "\n\t\t\t " ++ generate actions
	let resourceCode = generateResourceCode resources
	--putStrLn (show actions )
	let moduleDecl = "module Controller\nwhere"
	let methodDecl = "\nrequest url params sdata="
	putStrLn ("moduleDecl:" ++ "module Controller\nwhere")
	putStrLn ("\nimports :" ++ "\n" ++ imports)
	putStrLn ("methodDecl:" ++ "\nrequest url params sdata=")
	putStrLn ("gen :" ++ generated)
	putStrLn ("resources :" ++ resourceCode)
	writeFile "Controller.hs" (moduleDecl ++ "\n" ++ imports ++ methodDecl ++ generated ++ resourceCode)

generateImports [] = "import Utils"
generateImports (x:xs) = "import " ++ x ++ "\n" ++ generateImports xs

generateResourceCode [] = "return (\"No Mapping Found\",[])"
generateResourceCode (res:xs) = "if Utils.match url \""++res++"\" then return (\"RESOURCE\",[]) \n\t\t\telse " ++ generateResourceCode xs

generate [] = ""
generate l = generateAction results  ++ generate rest 
			where 
			combine [] = ([],[])
			combine [x,y] | (fst x) == (fst y) = ([x,y],[])
				      | otherwise = ([x],[y])
			combine (x:xs@(y:ys)) | (fst x) == (fst y) = let combined = combine xs in (x:(fst combined), (snd combined))
					      | otherwise = ([x],xs)
			combined = combine l
			results = fst combined
			rest = snd combined

--(generateAction x ++ "\n") ++ generate xs

generateAction l@((url,(mod,(method,(c,d)))):xs) = "if Utils.match url \"" ++ url ++ "\" then \n\t\t\t\t\tdo\n\t\t\t\t\t(res,params,sdata)<-"++mod++"."++ (if method /= "" then method else "execute") ++" params sdata"
				      ++"\n\t\t\t\t\tcase res of"
				      ++ getResultsCode l
				      ++ "\n\t\t\telse "

getResultsCode [] = ""
getResultsCode l@((url,(mod,(method,(c,d)))):xs) = "\n\t\t\t\t\t\t\""++c++"\"->"++"do\n\t\t\t\t\t\t\tout<-View.render \""++ d ++ "\" params sdata\n\t\t\t\t\t\t\treturn (out,sdata)" ++ getResultsCode xs 

--" url:" ++ show a ++ " action:" ++ show b ++ ":" ++ " resultValue: " ++ show c ++ " resultView:" ++ show d
