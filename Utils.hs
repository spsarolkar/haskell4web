module Utils
where
import Data.List.Split
import Data.List

match url pattern = 
		let patternList = splitOn "/" pattern
		    urlList = splitOn "/" url
		in
		matchList urlList patternList 

matchList [] [] = True
matchList x [] = True
matchList [] [y] = matchStr "" y
matchList [x] [y] = matchStr x y
matchList (url:xs) (pat:ys)  = if (matchStr url pat) then matchList xs ys else False

matchStr x y = ((x == y) || y == "*" || (((isInfixOf "." x) || (isInfixOf "." y)) && (extMatch x y)) )

extMatch url pat = let patList = splitOn "." pat
		       urlList = splitOn "." url
		   in matchList urlList patList 
		

--main = do
--putStrLn $ show $ splitOn "/" "home"
--	putStrLn $ show $ match "images" "images/*"
