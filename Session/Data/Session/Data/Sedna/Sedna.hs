module Session.Data.Sedna
where

import Database.SednaTypes
import Database.SednaBindings
import Prelude hiding (catch)
import Control.Exception
import Database.SednaBindings
import Database.Internal.SednaConnectionAttributes
import Text.XML.HXT.Core
import Data.Text.Internal
import qualified Data.Text as T
--import Data.ByteString.Char8   (pack, unpack)
import Data.Text
import Database.SednaExceptions
import Database.Internal.SednaConnectionAttributes
--

dbconnect = do
        let url="localhost"
        let dbname="sessions"
        let login="SYSTEM"
        let password="MANAGER"
        sednaConnect url dbname login password


data Session = Session String [(String,String)]

allSessions :: IO [Session]
allSessions = do
		sednaCon <- dbconnect
		catch (sednaBegin sednaCon) (\(e::SednaException) -> putStrLn $ show e)
		let selectAll = "let $d:=doc(\"sessions\")/sessions/session[@active=\"1\"] \n return $d"
		queryResult <- sednaGetResult sednaCon
		putStrLn ("Query Result:" ++ (unpack queryResult))
		let doc = readString [] ("<sessions>" ++ (unpack queryResult) ++ "</sessions>")
		sessions <- runX $ (doc) /> hasName "sessions" /> hasName "session" >>> ((getAttrValue "id") &&& ((this /> hasName "map") >>> (( this /> hasName "key" /> getText) &&& (this /> hasName "val" /> getText))))
		putStrLn $ show sessions
		return []

--getSession :: String -> Session
--getSession sessionId = 
